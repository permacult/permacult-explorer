export const environment = {
  production: true,

  apiUrl: 'https://api.openpermacult.org',

  gbifApi: 'https://api.gbif.org/v1',
};
