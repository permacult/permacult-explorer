import { TestBed } from '@angular/core/testing';

import { SearchTaxonomyService } from './search-taxonomy/search-taxonomy.service';

describe('SearchTaxonomyService', () => {
  let service: SearchTaxonomyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchTaxonomyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
