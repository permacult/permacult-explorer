/**
 * @description Uppercase accent safe first letter (default) see mode
 * @param str string
 * @param mode 'eachWord' | 'firstLetter' | 'firstChar' | 'firstWord' = 'firstLetter'
 * @param wordSeparator {regexp} /\s+/
 * @returns string
 */
export const ucFirst = (
  str: string,
  mode: 'eachWord' | 'firstLetter' | 'firstChar' | 'firstWord' = 'firstLetter',
  wordSeparator: RegExp | string = /\s+/,
) => {
  const letters = /[A-Za-zÀ-ÖØ-öø-ÿ]/;
  const ws =
    '^|' +
    (wordSeparator instanceof RegExp
      ? '(' + wordSeparator.source + ')'
      : // sanitize string for RegExp https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex#comment52837041_6969486
        '[' + wordSeparator.replace(/[[{}()*+?^$|\]\.\\]/g, '\\$&') + ']');

  const r =
    mode === 'firstLetter'
      ? letters
      : mode === 'firstChar'
      ? new RegExp('^' + letters.source)
      : mode === 'firstWord' || mode === 'eachWord'
      ? new RegExp(
          '(' + ws + ')' + letters.source,
          mode === 'eachWord' ? 'g' : undefined,
        )
      : undefined;

  if (r) {
    return str.replace(r, (c) => c.toUpperCase());
  } else {
    throw `error: ucFirst invalid mode (${mode}). Parameter should be one of: firstLetter|firstChar|firstWord|eachWord`;
  }
};
