import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'app-gbif-taxon-item-child',
  templateUrl: './gbif-taxon-item-child.component.html',
  styleUrls: ['./gbif-taxon-item-child.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonItemChildComponent implements OnInit {
  constructor() {}

  @Input()
  item: any;

  @Input()
  parent: any;

  @Input()
  showClassification: boolean = true;

  displayChildren = false;

  toggleDisplayChildren() {
    this.displayChildren = !this.displayChildren;
  }

  ngOnInit(): void {}
}
