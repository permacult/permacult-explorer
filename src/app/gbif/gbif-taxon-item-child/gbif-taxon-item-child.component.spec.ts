import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonItemChildComponent } from './gbif-taxon-item-child.component';

describe('GbifTaxonItemChildComponent', () => {
  let component: GbifTaxonItemChildComponent;
  let fixture: ComponentFixture<GbifTaxonItemChildComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonItemChildComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonItemChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
