import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonItemDescriptionComponent } from './gbif-taxon-item-description.component';

describe('GbifTaxonItemDescriptionComponent', () => {
  let component: GbifTaxonItemDescriptionComponent;
  let fixture: ComponentFixture<GbifTaxonItemDescriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonItemDescriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonItemDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
