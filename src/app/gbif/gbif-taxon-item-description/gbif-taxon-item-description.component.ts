import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { BehaviorSubject, of, throwError } from 'rxjs';
import {
  map,
  tap,
  distinctUntilChanged,
  switchMap,
  catchError,
} from 'rxjs/operators';
import { GbifService } from '../gbif.service';

@Component({
  selector: 'app-gbif-taxon-item-description',
  templateUrl: './gbif-taxon-item-description.component.html',
  styleUrls: ['./gbif-taxon-item-description.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonItemDescriptionComponent implements OnInit, OnChanges {
  constructor(private readonly service: GbifService) {}

  @Input()
  item: any;

  loading$ = new BehaviorSubject<boolean>(false);
  descriptions$ = new BehaviorSubject<any[]>([]);

  loadDescriptions(item: any = this.item) {
    this.loading$.next(true);
    this.service
      .findTaxonByIdDescriptions(this.item.key)
      .pipe(
        tap((r) => {
          this.loading$.next(false);
        }),
        map((r) => r.results),
        catchError((error) => {
          this.loading$.next(false);
          console.error(error);

          return of([]);
          // return throwError(
          //   error.message || 'Failed loading item descriptions'
          // );
        })
      )
      .subscribe((descriptions) => {
        this.descriptions$.next(descriptions);
      });
  }
  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.item && changes.item.currentValue.key) {
      this.loadDescriptions(changes.item.currentValue);
    }
  }
}
