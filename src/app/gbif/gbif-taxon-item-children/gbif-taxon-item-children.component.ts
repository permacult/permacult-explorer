import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { tick } from '@angular/core/testing';
import { BehaviorSubject, of, throwError } from 'rxjs';
import {
  startWith,
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
  finalize,
  catchError,
} from 'rxjs/operators';
import { GbifService } from '../gbif.service';

const defaultLimit = 20;
@Component({
  selector: 'app-gbif-taxon-item-children',
  templateUrl: './gbif-taxon-item-children.component.html',
  styleUrls: ['./gbif-taxon-item-children.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonItemChildrenComponent implements OnInit, OnChanges {
  constructor(private readonly service: GbifService) {}

  // itemChildren$ = new BehaviorSubject<any[]>([]);

  loading$ = new BehaviorSubject<boolean>(false);

  @Input()
  item: any;

  children$ = new BehaviorSubject<any[]>([]);
  childrenEndOfRecords$ = new BehaviorSubject<boolean>(false);

  limit: number = defaultLimit;
  offset: number = 0;

  loadChildren(
    item = this.item,
    { limit, offset }: { limit?: number; offset: number } = {
      limit: 20,
      offset: 0,
    }
  ) {
    if (item.key && item.numDescendants) {
      // console.log('item', this.item);

      this.loading$.next(true);

      this.service
        .findTaxonByIdChildren(item.key, { limit, offset })
        .pipe(
          tap((r) => {
            this.loading$.next(false);
            this.childrenEndOfRecords$.next(r.endOfRecords);
            this.limit = r.limit;
            this.offset = r.offset;
          }),
          map((r) => r.results),
          catchError((error) => {
            this.loading$.next(false);
            console.error(error);
            // return throwError(
            //   error.message || 'Failed loading item children'
            // );
            return of([]);
          })
        )
        .subscribe((children) => {
          this.children$.next(children);
        });
    }
  }

  loadMore(limit: number = defaultLimit) {
    this.loading$.next(true);

    this.service
      .findTaxonByIdChildren(this.item.key, {
        limit: limit,
        offset: this.limit + this.offset,
      })
      .pipe(
        tap((r) => {
          this.loading$.next(false);
          this.childrenEndOfRecords$.next(r.endOfRecords);
          this.limit = r.limit;
          this.offset = r.offset;
        }),
        map((r) => r.results)
      )
      .subscribe((children) => {
        this.children$
          .pipe(map((e) => e.push(...children)))
          .subscribe()
          .unsubscribe();
      });
  }

  ngOnInit(): void {
    this.loadChildren(this.item);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.item && changes.item.currentValue.key) {
      this.children$.next([]);
      this.childrenEndOfRecords$.next(true);

      this.loadChildren(this.item);
    }
  }
}
