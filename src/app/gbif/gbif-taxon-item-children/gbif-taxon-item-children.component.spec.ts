import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonItemChildrenComponent } from './gbif-taxon-item-children.component';

describe('GbifTaxonItemChildrenComponent', () => {
  let component: GbifTaxonItemChildrenComponent;
  let fixture: ComponentFixture<GbifTaxonItemChildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonItemChildrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonItemChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
