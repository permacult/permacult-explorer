import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonItemComponent } from './gbif-taxon-item.component';

describe('GbifTaxonItemComponent', () => {
  let component: GbifTaxonItemComponent;
  let fixture: ComponentFixture<GbifTaxonItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
