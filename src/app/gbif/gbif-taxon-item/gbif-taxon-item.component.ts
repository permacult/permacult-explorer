import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, of, throwError } from 'rxjs';
import {
  map,
  tap,
  distinctUntilChanged,
  switchMap,
  catchError,
} from 'rxjs/operators';
import { GbifService } from '../gbif.service';

@Component({
  selector: 'app-gbif-taxon-item',
  templateUrl: './gbif-taxon-item.component.html',
  styleUrls: ['./gbif-taxon-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonItemComponent implements OnInit {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly service: GbifService
  ) {}

  loading$ = new BehaviorSubject<boolean>(false);
  item$ = new BehaviorSubject<any>(null);

  ngOnInit(): void {
    this.loading$.next(true);
    this.route.params
      .pipe(
        distinctUntilChanged(),
        map((param) => param['id']),
        switchMap((id) => this.service.findTaxonById(id)),
        tap((e) => {
          this.loading$.next(false);
        }),
        tap((item) => {
          /**
           * load descriptions
           */
          // this.itemDescriptionsLoading$.next(true);
          // this.service
          //   .findTaxonByIdDescriptions(item.key)
          //   .pipe(
          //     tap((r) => {
          //       this.itemDescriptionsLoading$.next(false);
          //     }),
          //     map((r) => r.results),
          //     catchError((error) => {
          //       this.itemDescriptionsLoading$.next(false);
          //       console.error(error);
          //       return throwError(
          //         error.message || 'Failed loading item descriptions'
          //       );
          //     })
          //   )
          //   .subscribe((descriptions) => {
          //     this.itemDescriptions$.next(descriptions);
          //   });
        })
        // tap((item) => {
        //   /**
        //    * load children
        //    */
        //   this.itemChildrenLoading$.next(true);
        //   this.service
        //     .findTaxonByIdChildren(item.key)
        //     .pipe(
        //       tap((r) => {
        //         this.itemChildrenLoading$.next(false);
        //         this.itemChildrenEndOfRecords$.next(r.endOfRecords);
        //       }),
        //       map((r) => r.results),

        //       catchError((error) => {
        //         this.itemChildrenLoading$.next(false);
        //         console.error(error);
        //         return throwError(
        //           error.message || 'Failed loading item children'
        //         );
        //       })
        //     )
        //     .subscribe((children) => {
        //       this.itemChildren$.next(children);
        //     });
        // })
      )
      .subscribe((response) => {
        this.item$.next(response);
      });
  }
}
