import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifSearchComponent } from './gbif-search.component';

describe('GbifSearchComponent', () => {
  let component: GbifSearchComponent;
  let fixture: ComponentFixture<GbifSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
