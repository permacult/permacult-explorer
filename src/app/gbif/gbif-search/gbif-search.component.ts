import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { ucFirst } from 'src/app/shared/utils/uc-first';
import { GbifService } from '../gbif.service';
import {
  startWith,
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
  finalize,
  catchError,
} from 'rxjs/operators';
import { Habitat } from '../enums/habitat.enum';
import { Rank } from '../enums/rank.enum';
import { NameType } from '../enums/name-type.enum';
import { TaxonomicStatus } from '../enums/taxonomic-status.enum';

@Component({
  selector: 'app-gbif-search',
  templateUrl: './gbif-search.component.html',
  styleUrls: ['./gbif-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifSearchComponent implements OnInit {
  constructor(
    private readonly service: GbifService,
    private readonly fb: FormBuilder
  ) {}

  loading$ = new BehaviorSubject<boolean>(false);
  meta$ = new BehaviorSubject<{
    count: number;
    limit: number;
    offset: number;
    endOfRecords: boolean;
  } | null>(null);
  found$ = new BehaviorSubject<any[]>([]);
  search$ = new BehaviorSubject<any>({});

  fetch$ = new BehaviorSubject<any>({});

  math = Math;
  ucFirst = ucFirst;

  highertaxonKeyFormOptions: {
    option: string;
    value: number | null | undefined;
  }[] = [
    {
      option: 'Any',
      value: null,
    },
    {
      option: 'Plants',
      value: 6,
    },
    {
      option: 'Fungus',
      value: 5,
    },
    {
      option: 'Insects',
      value: 216,
    },
    {
      option: 'Bacteria',
      value: 3,
    },
    {
      option: 'Virus',
      value: 8,
    },
    {
      option: 'Animal',
      value: 1,
    },
    {
      option: 'Archaea',
      value: 2,
    },
  ];

  habitatFormOptions: {
    option: string;
    value: string | null | undefined;
  }[] = [{ option: 'Any', value: '' }].concat(
    Object.keys(Habitat).map((k) => {
      return {
        option: Habitat[k as keyof typeof Habitat],
        value: k,
      };
    })
  );

  rankFormOptions: {
    option: string;
    value: string | null | undefined;
  }[] = Object.keys(Rank).map((k) => {
    return {
      option: Rank[k as keyof typeof Rank],
      value: k,
    };
  });

  nameTypeFormOptions: {
    option: string;
    value: string | null | undefined;
  }[] = Object.keys(NameType).map((k) => {
    return {
      option: NameType[k as keyof typeof NameType],
      value: k,
    };
  });

  statusFormOptions: {
    option: string;
    value: string | null | undefined;
  }[] = Object.keys(TaxonomicStatus).map((k) => {
    return {
      option: TaxonomicStatus[k as keyof typeof TaxonomicStatus],
      value: k,
    };
  });

  form: FormGroup = this.fb.group({
    q: ['', []],
    highertaxonKey: [6, []],
    habitat: ['', []],
    rank: [
      [
        // 'VARIETY',
        // 'SUBVARIETY',
        // 'CONVARIETY',
        // 'SPECIES',
        // 'SUBSPECIES',
        // 'SUBGENUS',
        // 'GENUS',
        // 'CULTIVAR',
        // 'CULTIVAR_GROUP',
      ],
      [],
    ],
    nameType: [[], []],
    status: [['ACCEPTED'], []],
  });

  // VARIETY = 'variety',
  // SUBVARIETY = 'subvariety',
  // SPECIES = 'species',
  // SUBSPECIES = 'subspecies',

  // CONVARIETY = 'convariety',
  // GENUS = 'genus',
  // SUBGENUS = 'subgenus',

  // CULTIVAR = 'cultivar',
  // CULTIVAR_GROUP = 'cultivar group',

  search(search: any) {
    this.loading$.next(true);

    this.service
      .search(search)
      .pipe(
        tap((r) => {
          const { offset, limit, endOfRecords, count } = r;

          this.meta$.next({
            offset,
            limit,
            endOfRecords,
            count,
          });

          this.loading$.next(false);
        }),
        map((r) => r.results)
      )
      .subscribe((found) => {
        this.found$.next(found || []);
      });
  }

  ngOnInit(): void {
    this.form.valueChanges
      .pipe(
        // startWith(''),
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe((r) => {
        this.search(this.form.value);
      });
  }
}
