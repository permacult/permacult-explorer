import { LiteralPrimitive } from '@angular/compiler';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { GbifService } from '../gbif.service';

@Component({
  selector: 'app-gbif-taxon-list-item',
  templateUrl: './gbif-taxon-list-item.component.html',
  styleUrls: ['./gbif-taxon-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonListItemComponent implements OnInit, OnChanges {
  constructor(private readonly service: GbifService) {}

  @Input()
  item: any;

  @Input()
  loadChildren: boolean = false;

  toggleLoadChildren() {
    this.loadChildren = !this.loadChildren;
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    // if (this.itemMedia) return;

    if (changes.item && changes.item.currentValue.key) {
      // .pipe(
      //   tap((r) => {
      //     const { limit, offset, endOfRecords } = r;
      //     this.itemMediaMeta$ = of({
      //       limit,
      //       offset,
      //       endOfRecords,
      //     });
      //   }),
      //   map((r) => {
      //     return r.results || [];
      //   })
      // );
    }
  }

  // fetchTaxonMedia(id: number) {
  // if (this.items && this.items[id]) {
  //   return;
  // } else {
  //   this.mediaLoaded[id] = [];
  //   this.mediaLoaded[id] = await this.service
  //     .fetchTaxonMedia(id, { limit: 1 })
  //     .toPromise();
  // }
  // return this.mediaLoaded[id];
  // return this.service.fetchTaxonMedia(id, { limit: 1 });
  // .then((r) => (Array.isArray(r) ? r : []));
  // .subscribe((r) => (Array.isArray(r) ? r : []));
  // .pipe(
  //   map((r) => {
  //     // return Array.isArray(r) ? r : [];
  //     return r;
  //   })
  // );
  // console.log('fetch');
  // return of([
  //   {
  //     identifier: 'koko',
  //   },
  // ]);
  // }
}
