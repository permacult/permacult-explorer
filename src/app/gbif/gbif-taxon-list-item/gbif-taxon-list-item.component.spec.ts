import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonListItemComponent } from './gbif-taxon-list-item.component';

describe('GbifTaxonListItemComponent', () => {
  let component: GbifTaxonListItemComponent;
  let fixture: ComponentFixture<GbifTaxonListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
