import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GbifTaxonListComponent } from './gbif-taxon-list/gbif-taxon-list.component';
import { GbifSearchComponent } from './gbif-search/gbif-search.component';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { AppRoutingModule } from '../app-routing.module';
// import { GbifTaxonListItemComponent } from './gbif-taxon-list-item/gbif-taxon-list-item.component';
import { GbifTaxonItemComponent } from './gbif-taxon-item/gbif-taxon-item.component';
import { GbifTaxonItemClassificationComponent } from './gbif-taxon-item-classification/gbif-taxon-item-classification.component';
import { GbifTaxonItemChildComponent } from './gbif-taxon-item-child/gbif-taxon-item-child.component';
import { GbifTaxonItemChildrenComponent } from './gbif-taxon-item-children/gbif-taxon-item-children.component';
import { MatBadgeModule } from '@angular/material/badge';
import { GbifTaxonItemDescriptionComponent } from './gbif-taxon-item-description/gbif-taxon-item-description.component';
import { GbifTaxonItemMediaComponent } from './gbif-taxon-item-media/gbif-taxon-item-media.component';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [
    GbifSearchComponent,
    GbifTaxonListComponent,
    // GbifTaxonListItemComponent,
    GbifTaxonItemComponent,
    GbifTaxonItemClassificationComponent,
    GbifTaxonItemChildrenComponent,
    GbifTaxonItemChildComponent,
    GbifTaxonItemDescriptionComponent,
    GbifTaxonItemMediaComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    MatIconModule,
    MatToolbarModule,
    MatBadgeModule,
    MatMenuModule,
    MatDialogModule,
    MatDividerModule,
    LazyLoadImageModule,
  ],
})
export class GbifModule {}
