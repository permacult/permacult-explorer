import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { ucFirst } from 'src/app/shared/utils/uc-first';

@Component({
  selector: 'app-gbif-taxon-item-classification',
  templateUrl: './gbif-taxon-item-classification.component.html',
  styleUrls: ['./gbif-taxon-item-classification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonItemClassificationComponent implements OnInit {
  constructor() {}

  @Input()
  item: any;

  @Input()
  parent: any;

  @Input()
  short: boolean = false;

  ucFirst = ucFirst;

  itemClassifications(item = this.item): {
    rank: string;
    name: string;
    key: number;
    isItemParent: boolean;
    isItemCurrent: boolean;
  }[] {
    const ranks = [
      'kingdom',
      'phylum',
      'class',
      'order',
      'family',
      'genus',
      'parent',
    ];

    const keys: number[] = [];
    const results: {
      rank: string;
      name: string;
      key: number;
      isItemParent: boolean;
      isItemCurrent: boolean;
    }[] = [];

    for (const rank of ranks) {
      if (item[rank + 'Key'] && !keys.includes(item[rank + 'Key'])) {
        const key = item[rank + 'Key'];
        const name = item[rank];

        keys.push(key);

        results.push({
          rank,
          key,
          name,
          isItemParent: item.parentKey == key,
          isItemCurrent: item.key == key,
        });
      }
    }

    if (this.short) {
      return results.filter((e) => !e.isItemCurrent).slice(-1);
    }

    return results;
  }

  btnNgClass(
    { rank, name, key }: { rank: string; name: string; key: number },
    item = this.item
  ) {
    let result = {
      ['rank-' + rank]: true,
      'md-small': true,
      'is-item-parent': key == item.parentKey,
      'is-item-current': key == item.key,
    };

    return result;
  }

  ngOnInit(): void {}
}
