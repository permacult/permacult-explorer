import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonItemClassificationComponent } from './gbif-taxon-item-classification.component';

describe('GbifTaxonItemClassificationComponent', () => {
  let component: GbifTaxonItemClassificationComponent;
  let fixture: ComponentFixture<GbifTaxonItemClassificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonItemClassificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonItemClassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
