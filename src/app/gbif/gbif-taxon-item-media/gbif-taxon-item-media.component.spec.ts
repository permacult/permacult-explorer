import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonItemMediaComponent } from './gbif-taxon-item-media.component';

describe('GbifTaxonItemMediaComponent', () => {
  let component: GbifTaxonItemMediaComponent;
  let fixture: ComponentFixture<GbifTaxonItemMediaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonItemMediaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonItemMediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
