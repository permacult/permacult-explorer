import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ShadowBoxDialogComponent } from 'src/app/shadow-box-dialog/shadow-box-dialog.component';
import { GbifService } from '../gbif.service';

export interface ShadowBoxImage {
  src: string;
  title?: string;
  description?: string;
}

@Component({
  selector: 'app-gbif-taxon-item-media',
  templateUrl: './gbif-taxon-item-media.component.html',
  styleUrls: ['./gbif-taxon-item-media.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonItemMediaComponent implements OnInit, OnChanges {
  constructor(
    private readonly service: GbifService,
    public dialog: MatDialog
  ) {}

  @Input()
  item: any;

  @Input()
  limit: number = 10;

  @Input()
  offset: number = 0;

  itemMedia$ = new BehaviorSubject<any[]>([]);
  loading$ = new BehaviorSubject<boolean>(false);

  mediaMeta:
    | { limit: number; offset: number; endOfRecords: boolean }
    | undefined;

  mediaTitle(media: any) {
    return (
      [media.description, media.creator, media.publisher, media.rightHolder]
        .filter((e) => e && !e.match(/^\s*$/)) // filter empty elements
        .join(' - ') || ''
    );
  }

  encodeURIComponent(string: string) {
    return encodeURIComponent(string);
  }

  openShadowBoxDialog(image: ShadowBoxImage) {
    this.dialog.open(ShadowBoxDialogComponent, {
      data: image,
    });
  }

  loadItemMedia(item = this.item) {
    this.loading$.next(true);
    this.itemMedia$.next([]);

    // console.log('fetchTaxonMedia', )
    this.service
      .fetchTaxonMedia(item.key, {
        limit: this.limit,
        offset: this.offset,
      })
      .pipe(
        tap((r) => {
          const { limit, offset, endOfRecords } = r;

          this.mediaMeta = { limit, offset, endOfRecords };
        }),
        tap((r) => {
          this.loading$.next(false);
        }),
        map((r) => r.results),
        catchError((error) => {
          this.loading$.next(false);
          console.error(error);

          return of([]);
          // return throwError(error.message || 'server error.');
        })
      )
      .subscribe((taxons) => {
        let itemMedia: any[] = [];

        taxons
          .filter((t: any) => t.media)
          .forEach((taxon: any) => {
            itemMedia.push(...taxon.media);
          });
        console.log('itemMedia', itemMedia);
        this.itemMedia$.next(
          itemMedia.filter((t: any) => t.identifier).slice(0, 10)
        );
      });
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.item && changes.item.currentValue.key) {
      this.loadItemMedia(changes.item.currentValue);
    }
  }
}
