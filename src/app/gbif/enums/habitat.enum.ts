export enum Habitat {
  FRESHWATER = 'Freshwater',
  // Freshwater habitats including rivers, lakes, ponds, wetlands, bogs, marsh, swamp and brackish waters!
  MARINE = 'Marine',
  // Marine habitats including the deep & open ocean, reefs and estuaries.
  TERRESTRIAL = 'Terrestrial',
  // Terrestrial habitats cover all habitats on land including forests, deserts, grasslands, meadows, tundra, mangroves, farmland.
}
