/**
 * https://gbif.github.io/gbif-api/apidocs/org/gbif/api/vocabulary/NameType.html
 */

export enum NameType {
  BLACKLISTED = 'Blacklisted',
  CULTIVAR = 'Cultivar',
  HYBRID = 'A hybrid formula (not a hybrid name).',
  SCIENTIFIC = 'Scientific',
  VIRUS = 'Virus',
}
