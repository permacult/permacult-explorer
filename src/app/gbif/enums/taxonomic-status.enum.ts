/**
 * https://gbif.github.io/gbif-api/apidocs/org/gbif/api/vocabulary/TaxonomicStatus.html
 */

export enum TaxonomicStatus {
  ACCEPTED = 'Accepted',
  DOUBTFUL = 'Treated as accepted, but doubtful whether this is correct.',
  HETEROTYPIC_SYNONYM = 'Heterotypic synonym',
  HOMOTYPIC_SYNONYM = 'Homotypic synonym',
  MISAPPLIED = 'Misapplied synonym',
  PROPARTE_SYNONYM = 'Proparte synonym',
  SYNONYM = 'A general synonym, the exact type is unknown.',
}
