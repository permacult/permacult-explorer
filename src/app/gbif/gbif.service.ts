import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Rank } from './enums/rank.enum';

export class GbifSearchParam {
  // see https://www.gbif.org/developer/species#searching
  q?: string;
  datasetKey?: string;
  rank?: Rank;
  highertaxonKey?: number;
  status?: string;
  isExtinct?: boolean;
  habitat?: string;
  threat?: string;
  nameType?: string;
  nomenclaturalStatus?: string;
  issue?: string;
  hl?: any;
  facet?: any;
  facetMincount?: any;
  facetMultiselect?: any;
}

@Injectable({
  providedIn: 'root',
})
export class GbifService {
  constructor(private readonly http: HttpClient) {}

  private readonly speciesApi = environment.gbifApi + '/species';

  search(search: GbifSearchParam): Observable<any> {
    let params = new HttpParams();

    const q = (search.q || '').replace(/^[\s]+|[\s]+$/g, '');

    if (q == '') return of([]);

    params = params.append('q', q);

    if (search.highertaxonKey) {
      params = params.append('highertaxonKey', search.highertaxonKey);
    }

    if (search.habitat) {
      params = params.append('habitat', search.habitat);
    }

    if (search.rank) {
      (Array.isArray(search.rank) ? search.rank : [search.rank]).forEach(
        (rank) => {
          params = params.append('rank', rank);
        }
      );
    }

    if (search.nameType) {
      (Array.isArray(search.nameType)
        ? search.nameType
        : [search.nameType]
      ).forEach((nameType) => {
        params = params.append('nameType', nameType);
      });
    }

    if (search.status) {
      (Array.isArray(search.status) ? search.status : [search.status]).forEach(
        (status) => {
          params = params.append('status', status);
        }
      );
    }

    try {
      return this.http.get<any>(`${this.speciesApi}/search`, {
        params,
      });
    } catch (e) {
      console.error(e);
    }

    return of([]);
  }

  fetchTaxonMedia(
    id: number,
    { limit, offset }: { limit?: number; offset?: number } = {
      limit: 20,
      offset: 0,
    }
  ): Observable<any> {
    try {
      // return this.http.get<any>(
      //   `${this.speciesApi}/${id}/media?limit=10&media_type=stillImage`
      // );

      return this.http.get<any>(
        // `${this.speciesApi}/${id}/media?limit=10&media_type=stillImage`
        'https://api.gbif.org/v1/occurrence/search?limit=10&media_type=stillImage&taxon_key=' +
          id
      );
    } catch (e) {
      console.error(e);
    }
    //https://api.gbif.org/v1/occurrence/search?limit=20&media_type=stillImage&taxon_key=7317523

    return of([]);
  }

  findTaxonById(id: number): Observable<any> {
    try {
      return this.http.get<any>(`${this.speciesApi}/${+id}`);
    } catch (e) {
      console.error(e);
    }

    return of([]);
  }

  findTaxonByIdDescriptions(id: number): Observable<any> {
    try {
      return this.http.get<any>(`${this.speciesApi}/${+id}/descriptions`);
    } catch (e) {
      console.error(e);
    }

    return of([]);
  }

  findTaxonByIdChildren(
    id: number,
    { limit, offset }: { limit?: number; offset?: number } = {
      limit: 20,
      offset: 0,
    }
  ): Observable<any> {
    // if (+id > 0) {
    try {
      return this.http.get<any>(
        `${this.speciesApi}/${+id}/children?limit=${limit}&offset=${offset}`
      );
    } catch (e) {
      console.error(e);
    }
    // }

    return of({});
  }
}
