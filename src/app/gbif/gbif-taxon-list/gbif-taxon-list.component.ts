import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { GbifService } from '../gbif.service';

@Component({
  selector: 'app-gbif-taxon-list',
  templateUrl: './gbif-taxon-list.component.html',
  styleUrls: ['./gbif-taxon-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GbifTaxonListComponent implements OnInit {
  constructor(private readonly service: GbifService) {}

  @Input()
  loading: boolean | null = null;

  @Input()
  meta: any;

  @Input()
  items: any[] | null = [];

  // mediaLoaded: { [key: number]: any[] } = [];

  ngOnInit(): void {}

  /**
   * https://stackoverflow.com/questions/70281750/easy-angular-way-to-detect-if-element-is-in-viewport-on-scroll
   */
  /*
      // the element that you are observing (just add #yourElement in the template) for this  query to work
      @ViewChild('yourElement') yourElement: ElementRef;

      ngAfterViewInit() {
          const threshold = 0.2; // how much % of the element is in view
          const observer = new IntersectionObserver(
              (entries) => {
                  entries.forEach((entry) => {
                      if (entry.isIntersecting) {
                          // run your animation code here
                          observer.disconnect(); // disconnect if you want to stop observing else it will rerun every time its back in view. Just make sure you disconnect in ngOnDestroy instead
                      }
                  });
              },
              { threshold }
          );
          observer.observe(this.yourElement.nativeElement);
      }
      */
}
