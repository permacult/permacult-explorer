import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbifTaxonListComponent } from './gbif-taxon-list.component';

describe('GbifTaxonListComponent', () => {
  let component: GbifTaxonListComponent;
  let fixture: ComponentFixture<GbifTaxonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbifTaxonListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbifTaxonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
