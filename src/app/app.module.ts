import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import {
  FormsModule,
  NG_VALUE_ACCESSOR,
  ReactiveFormsModule,
} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchTaxonomyComponent } from './search-taxonomy/search-taxonomy.component';
import { TaxNodeListItemComponent } from './tax-node-list-item/tax-node-list-item.component';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { TaxNodeComponent } from './tax-node/tax-node.component';
import { HomeComponent } from './home/home.component';
import { GbifModule } from './gbif/gbif.module';
import { ShadowBoxDialogComponent } from './shadow-box-dialog/shadow-box-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CultComponent } from './cult/cult.component';
import { VisGraphComponent } from './cult/vis-graph/vis-graph.component';

@NgModule({
  declarations: [
    AppComponent,
    TaxNodeListItemComponent,
    SearchTaxonomyComponent,
    TaxNodeComponent,
    HomeComponent,
    ShadowBoxDialogComponent,
    CultComponent,
    VisGraphComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    MatIconModule,
    MatToolbarModule,
    MatDividerModule,
    LazyLoadImageModule,

    MatDialogModule,
    GbifModule,
  ],
  // exports: [MatCardModule],
  // providers: [],
  // providers: [
  //   {
  //     provide: NG_VALUE_ACCESSOR,
  //     multi: true,
  //     useExisting: MyComponent,
  //   }
  // ],
  bootstrap: [AppComponent],
})
export class AppModule {}
