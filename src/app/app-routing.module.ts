import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisGraphComponent } from './cult/vis-graph/vis-graph.component';
import { GbifSearchComponent } from './gbif/gbif-search/gbif-search.component';
import { GbifTaxonItemComponent } from './gbif/gbif-taxon-item/gbif-taxon-item.component';
import { HomeComponent } from './home/home.component';
import { SearchTaxonomyComponent } from './search-taxonomy/search-taxonomy.component';
import { TaxNodeListItemComponent } from './tax-node-list-item/tax-node-list-item.component';
import { TaxNodeComponent } from './tax-node/tax-node.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'search',
    component: SearchTaxonomyComponent,
  },
  {
    path: 'nlm/tax/node/:id',
    component: TaxNodeComponent,
  },
  {
    path: 'gbif/search',
    component: GbifSearchComponent,
  },
  {
    path: 'gbif/species/:id',
    component: GbifTaxonItemComponent,
  },
  {
    path: 'cult/vis-graph',
    component: VisGraphComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
