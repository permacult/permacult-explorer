import { Component, Input, OnInit } from '@angular/core';
import { taxNodeTomato } from 'src/mockups/tax-node-tomato';
import { uniq } from 'lodash';
import { TaxNodeService } from '../tax-node/tax-node.service';
import { map, tap } from 'rxjs/operators';

const commonNameClasses = ['common name', 'genbank common name', 'blast name'];
const scientificNameClasses = ['scientific name', 'synonym'];

@Component({
  selector: 'app-tax-node-list-item',
  templateUrl: './tax-node-list-item.component.html',
  styleUrls: ['./tax-node-list-item.component.scss'],
})
export class TaxNodeListItemComponent implements OnInit {
  constructor(private service: TaxNodeService) {}

  @Input()
  taxNode: any;

  ngOnInit(): void {}

  trim(str: string): string {
    return str
      .replace(/\svar\.\s.*$/, '') // remove trailing 'var. %variety%'
      .replace(/^[\-\._"'\s,]+|[\-_"'\s,]+$/g, ''); // preserve trailing dot (.) at the end
  }

  encodeURI(str: string): string {
    return encodeURIComponent(str);
  }

  windowOpen(url: string, target: string = '_blank'): Window | null {
    return window.open(url, target);
  }

  loadChild(node: any, event: Event): void {
    // node.children
    node.childrenLoaded = false;

    const children = this.service
      .findById(node.id)
      .pipe(map((e) => e.children))
      .subscribe((e) => {
        node.childrenLoaded = true;
        node.children = e;
      });
    // console.log(children);
  }

  public removeChildren(node: any): void {
    try {
      delete node.childrenLoaded;
      delete node.children;
    } catch (err) {
      console.error(err);
    }
  }

  // public readonly taxNode: any = taxNodeTomato;

  public concatNames(names: any[]): string[] {
    return Array.isArray(names) && names.filter((e) => e).length
      ? names.map((n) => `${n.class}: ${n.name}`)
      : [];
  }

  public commonNames(names: any[]): string[] {
    return Array.isArray(names) && names.filter((e) => e).length
      ? uniq(
          names
            .filter((n) => commonNameClasses.includes(n.class))
            .map((n) => n.name)
            .sort((a, b) => b.length - a.length) // sort by shortest first
        )
      : [];
  }

  public scientificNames(names: any[]): string[] {
    return Array.isArray(names) && names.filter((e) => e).length
      ? uniq(
          names
            .filter((n) => scientificNameClasses.includes(n.class))
            .map((n) => n.name) // get the name
            .sort((a, b) => a.length - b.length) // sort by length
        )
      : [];
  }

  public filterHasCommonName(nodes: any[]): any[] {
    return Array.isArray(nodes) && nodes.filter((e) => e && e.names).length
      ? nodes.filter((node: any) =>
          node.names.some((name: any) => commonNameClasses.includes(name.class))
        )
      : [];
  }
}
