import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxNodeListItemComponent } from './tax-node-list-item.component';

describe('TaxNodeListItemComponent', () => {
  let component: TaxNodeListItemComponent;
  let fixture: ComponentFixture<TaxNodeListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxNodeListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxNodeListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
