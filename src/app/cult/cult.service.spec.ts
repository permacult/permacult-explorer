import { TestBed } from '@angular/core/testing';

import { CultService } from './cult.service';

describe('CultService', () => {
  let service: CultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
