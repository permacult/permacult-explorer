import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CultService {
  constructor(private readonly http: HttpClient) {}

  getAllNodes(): Observable<any[]> {
    let params = new HttpParams();

    try {
      return this.http.get<any[]>(`${environment.cultApi}/nodes`, {
        params,
      });
    } catch (e) {
      console.error(e);
    }

    return of([]);
  }

  getAllEdges(): Observable<any[]> {
    let params = new HttpParams();

    try {
      return this.http.get<any[]>(`${environment.cultApi}/edges`, {
        params,
      });
    } catch (e) {
      console.error(e);
    }

    return of([]);
  }
}
