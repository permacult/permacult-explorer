import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { forkJoin } from 'rxjs';
import { CultService } from '../cult.service';

import * as jquery from 'jquery';

import * as cytoscape from 'cytoscape';
import * as edgehandles from 'cytoscape-edgehandles';
import * as cxtmenu from 'cytoscape-cxtmenu';
import * as contextMenus from 'cytoscape-context-menus';

// import * as edgeBendEditing from 'cytoscape-edge-editing';
// const cytoscape = require('cytoscape');
// const edgehandles = require('cytoscape-edgehandles')

cytoscape.use(edgehandles);
cytoscape.use(contextMenus);
cytoscape.use(cxtmenu);

const edgeEditing = require('cytoscape-edge-editing');
edgeEditing(cytoscape, jquery);
cytoscape.use(edgeEditing);

@Component({
  selector: 'app-vis-graph',
  templateUrl: './vis-graph.component.html',
  styleUrls: ['./vis-graph.component.scss'],
})
export class VisGraphComponent implements OnInit {
  constructor(private readonly service: CultService) {}

  // private cy: cy.Core;

  // @ViewChild('cyGraph')
  // cyGraph: ElementRef;

  cy: cytoscape.Core | undefined;

  ngOnInit(): void {
    // this.cy = cytoscape({
    //   container: document.getElementById('cy'),
    // });

    const nodes$ = this.service.getAllNodes();
    const edges$ = this.service.getAllEdges();

    forkJoin([nodes$, edges$]).subscribe(([nodes, edges]) => {
      // here is how it should be done
      // https://github.com/calvinvette/ngx-cytoscape/blob/master/src/cytoscape.ts
      // I was missing the
      // // set the current graph elem collection layout to current instance value.
      // let layout = nodes.layout(this.layout);

      // // we need to rerun the layout again
      // layout.run();

      const elements: cytoscape.ElementDefinition[] = [];

      elements.push(
        ...nodes.map((e) => {
          return <cytoscape.ElementDefinition>{
            data: {
              id: 'n' + e.id,
              name: e.name,
              image: e.name === 'Tomatoes' ? '/assets/tomato.jpg' : undefined, //e.featuredMedia?.url || undefined,
            },
            selectable: true,
          };
        })
      );

      for (const edge of edges) {
        elements.push(<cytoscape.ElementDefinition>{
          group: 'edges',
          classes: 'raw-edge',
          data: {
            // id: `n${e.nodeId}:n${e.revNodeId}`,
            id: 'e' + edge.id,

            source: 'n' + edge.nodeId,
            target: 'n' + edge.revNodeId,

            edgeId: 'e' + edge.id,
            type: edge.type,
            name: edge.name,
          },
        });

        for (const content of edge.contents) {
          elements.push(<cytoscape.ElementDefinition>{
            group: 'edges',
            classes: content.type.split('.').join(' '),
            data: {
              // id: `n${e.nodeId}:n${e.revNodeId}`,
              id: content.id,
              source: 'n' + edge.nodeId,
              target: 'n' + edge.revNodeId,

              edgeId: 'e' + edge.id,

              type: edge.type,
              name: edge.name,
              classes: content.type.split('.').join(' '),
            },
          });
        }
      }

      // elements.push(
      //   ...edges
      //     // .filter((e) =>
      //     //   e.contents.some((c: any) => /^action\.help/.test(c.type))
      //     // )
      //     .map((e) => {
      //       const classes = [
      //         // let uniq = a => [...new Set(a)];
      //         ...new Set(
      //           (e.contents || []) // loop over contents
      //             .map((e: any) => e.type.split('.')) // ['action.help'] => [['action', 'help']]
      //             .flat(1) // ['action.help', 'avoid'] => [['action', 'help'],['avoid']] => ['action', 'help' ,'avoid']]
      //         ),
      //       ].join(' '); // contents[].type=['action.help','avoid'] => 'action help avoid'

      //       return <cytoscape.ElementDefinition>{
      //         group: 'edges',
      //         // selectable: true,
      //         // grabbable: true,

      //         // classes: 'action help',

      //         classes,

      //         // style: {
      //         //   width: 10,
      //         //   'line-color': 'rgba(20,180,20,1)',
      //         //   'target-arrow-color': 'rgba(20,180,20,1)',
      //         //   'target-arrow-shape': 'triangle',
      //         //   'curve-style': 'bezier',
      //         //   label: 'data(name)',
      //         // },
      //         data: {
      //           // id: `n${e.nodeId}:n${e.revNodeId}`,
      //           id: 'e' + e.id,
      //           source: 'n' + e.nodeId,
      //           target: 'n' + e.revNodeId,
      //           type: e.type,
      //           classes,
      //           name: [
      //             e.name,
      //             ...e.contents.map(
      //               (e: any) => `${e.type} ${e.title || e.html || ''}`
      //             ),
      //           ].join(' '),
      //         },
      //       };
      //     })
      // );

      // for (const e in elements) {
      //   this.cy.add(e);
      // }
      // console.log('elements', elements);

      // this.cy.edgehandles({
      //   preview: true,
      //   stackOrder: 4,
      //   handleSize: 2,
      //   handleHitThreshold: 0,
      //   handleIcon: true,
      //   handleLineType: 'ghost',
      //   handleNodes: 'node',
      //   handlePosition: 'right bottom',
      //   hoverDelay: 3,
      //   cxt: true,
      //   toggleOffOnLeave: true,
      //   edgeType: function () {
      //     return 'flat';
      //   },
      // });

      let layoutRandom = {
        name: 'random',

        fit: true, // whether to fit to viewport
        padding: 30, // fit padding
        boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        animate: false, // whether to transition the node positions
        animationDuration: 500, // duration of animation in ms if enabled
        animationEasing: undefined, // easing of animation if enabled
        animateFilter: function (node: any, i: any) {
          return true;
        }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
        ready: undefined, // callback on layoutready
        stop: undefined, // callback on layoutstop
        transform: function (node: any, position: any) {
          return position;
        }, // transform a given node position. Useful for changing flow direction in discrete layouts
      };

      let layoutCercle = {
        name: 'circle',

        fit: true, // whether to fit the viewport to the graph
        padding: 30, // the padding on fit
        boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        avoidOverlap: true, // prevents node overlap, may overflow boundingBox and radius if not enough space
        nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
        spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
        radius: undefined, // the radius of the circle
        startAngle: (3 / 2) * Math.PI, // where nodes start in radians
        sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
        clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
        sort: undefined, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }
        animate: false, // whether to transition the node positions
        animationDuration: 500, // duration of animation in ms if enabled
        animationEasing: undefined, // easing of animation if enabled
        animateFilter: function (node: any, i: any) {
          return true;
        }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
        ready: undefined, // callback on layoutready
        stop: undefined, // callback on layoutstop
        transform: function (node: any, position: any) {
          return position;
        }, // transform a given node position. Useful for changing flow direction in discrete layouts
      };

      let layoutConcentric = {
        name: 'concentric',

        fit: true, // whether to fit the viewport to the graph
        padding: 50, // the padding on fit
        startAngle: 1 * Math.PI, // where nodes start in radians
        sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
        clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
        equidistant: false, // whether levels have an equal radial distance betwen them, may cause bounding box overflow
        minNodeSpacing: 100, // min spacing between outside of nodes (used for radius adjustment)
        boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
        nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
        height: undefined, // height of layout area (overrides container height)
        width: undefined, // width of layout area (overrides container width)
        spacingFactor: 1, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
        concentric: function (node: any) {
          // returns numeric value for each node, placing higher nodes in levels towards the centre
          return node.degree();
        },
        levelWidth: function (nodes: any) {
          // the variation of concentric values in each level
          return nodes.maxDegree() / 8;
        },
        animate: false, // whether to transition the node positions
        animationDuration: 500, // duration of animation in ms if enabled
        animationEasing: undefined, // easing of animation if enabled
        animateFilter: function (node: any, i: any) {
          return true;
        }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
        ready: undefined, // callback on layoutready
        stop: undefined, // callback on layoutstop
        transform: function (node: any, position: any) {
          return position;
        }, // transform a given node position. Useful for changing flow direction in discrete layouts
      };

      let layoutBreadthfirst = {
        name: 'breadthfirst',

        fit: true, // whether to fit the viewport to the graph
        directed: true, // whether the tree is directed downwards (or edges can point in any direction if false)
        padding: 200, // padding on fit
        circle: true, // put depths in concentric circles if true, put depths top down if false
        grid: false, // whether to create an even grid into which the DAG is placed (circle:false only)
        spacingFactor: 5, // positive spacing factor, larger => more space between nodes (N.B. n/a if causes overlap)
        boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
        nodeDimensionsIncludeLabels: true, // Excludes the label when calculating node bounding boxes for the layout algorithm
        roots: undefined, // the roots of the trees
        maximal: false, // whether to shift nodes down their natural BFS depths in order to avoid upwards edges (DAGS only)
        depthSort: undefined, // a sorting function to order nodes at equal depth. e.g. function(a, b){ return a.data('weight') - b.data('weight') }
        animate: true, // whether to transition the node positions
        animationDuration: 500, // duration of animation in ms if enabled
        animationEasing: undefined, // easing of animation if enabled,
        animateFilter: function (node: any, i: any) {
          return true;
        }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
        ready: undefined, // callback on layoutready
        stop: undefined, // callback on layoutstop
        transform: function (node: any, position: any) {
          return position;
        }, // transform a given node position. Useful for changing flow direction in discrete layouts
      };

      this.cy = cytoscape({
        container: document.getElementById('cy'),
        elements,
        layout: layoutConcentric,
        // layout: {
        //   name: 'grid',
        //   // directed: false,
        //   fit: true,
        //   avoidOverlap: true,
        //   // cols: 4,
        //   animate: true,
        //   animationDuration: 300,

        //   // animationEase: 'ease-in-out'
        // },
        style: [
          // the stylesheet for the graph
          {
            selector: 'node',
            style: {
              'background-color': '#777',
              label: 'data(name)',
              width: 100,
              height: 100,
              'border-color': '#ccc',
            },
          },
          {
            selector: 'node[image]',
            style: {
              label: 'data(name)',

              'background-image': 'data(image)',
              'background-fit': 'cover',
              'background-opacity': 0, // do not show the bg color
              // 'background-clip': 'none', // let image go beyond node shape (also better performance)
            },
          },

          {
            selector: 'edge',
            style: {
              width: 3,
              'line-color': '#b0b0b0',
              // 'target-arrow-color': '#ccc',
              // 'target-arrow-shape': 'vee',
              // 'curve-style': 'bezier',
              // label: 'data(name)',
              // 'z-index': -10,
            },
          },

          // {
          //   selector: 'edge[name]',
          //   style: {
          //     // width: 10,
          //     label: 'data(name)',
          //   },
          // },

          {
            selector: 'edge[classes]',
            style: {
              width: 10,
              // 'line-color': 'rgba(20,180,20,1)',
              // 'target-arrow-color': 'rgba(20,180,20,1)',
              'target-arrow-shape': 'triangle',
              'curve-style': 'bezier',
              // 'curve-style': 'unbundled-bezier',
              // 'line-dash-pattern': [30, 20],
              // 'line-dash-offset': 10,
              label: 'data(classes)',
              'z-index': 10,
            },
          },

          {
            selector: 'edge.action.help',
            style: {
              width: 5,
              'line-color': 'rgba(20,180,20,1)',
              'target-arrow-color': 'rgba(20,180,20,1)',
              // 'target-arrow-shape': 'triangle',
              // 'curve-style': 'bezier',
              // 'curve-style': 'unbundled-bezier',
              // 'line-dash-pattern': [30, 20],
              // 'line-dash-offset': 10,
              // label: 'data(classes)',
            },
          },
          {
            selector: 'edge.avoid',
            style: {
              width: 5,
              'line-color': 'rgba(180,20,20,1)',
              'target-arrow-color': 'rgba(180,20,20,1)',
              // 'target-arrow-shape': 'triangle',
              // 'curve-style': 'unbundled-bezier',
              // 'line-dash-pattern': [30, 20],
              // 'line-dash-offset': 10,
              // 'curve-style': 'bezier',
              label: 'data(classes)',
              // 'z-index': 10,
            },
          },

          // edge handles
          {
            selector: '.edgehandles-hover',
            css: {
              'background-color': 'red',
            },
          },
          {
            selector: '.edgehandles-source',
            css: {
              'border-width': 2,
              'border-color': 'red',
            },
          },
          {
            selector: '.edgehandles-target',
            css: {
              'border-width': 2,
              'border-color': 'red',
            },
          },
          {
            selector: '.edgehandles-preview, .edgehandles-ghost-edge',
            css: {
              'line-color': 'red',
              'target-arrow-color': 'red',
              'source-arrow-color': 'red',
            },
          },
        ],
      });

      // this.cy.add(elements);

      // this.cy.edgehandles({
      //   preview: true,
      //   stackOrder: 4,
      //   handleSize: 2,
      //   handleHitThreshold: 0,
      //   // handleIcon: true,
      //   // handleLineType: 'ghost',
      //   // handleNodes: 'node',
      //   // handlePosition: 'right bottom',
      //   // hoverDelay: 3,
      //   // cxt: true,
      //   toggleOffOnLeave: true,
      //   edgeType: function () {
      //     return 'flat';
      //   },
      // });

      // this.cy.layout(options);

      this.cy.on('tap', 'edge', (e: any) => {
        const edge = e.target;
        // console.log('edge', edge);
        console.log('edge.data', edge.data());
      });

      // this.cy.on('drag', 'node', function (e: any) {
      //   e.target.style({
      //     shape: 'star',
      //     'background-color': 'yellow',
      //     color: 'black',
      //   });
      // });

      // this.cy.on('free', 'node', function () {
      //   this.style({
      //     'background-color': 'pink',
      //     color: '#a1248d',
      //     label: 'Dragged',
      //     width: '70px',
      //     height: '50px',
      //     'text-outline-width': 1,
      //   });
      //   this.connectedEdges().style({
      //     'line-color': 'pink',
      //     'target-arrow-color': 'pink',
      //     'curve-style': 'unbundled-bezier',
      //     'line-style': 'dashed',
      //   });
      // });

      let edgehandlesDefaults: edgehandles.EdgeHandlesOptions = {
        canConnect: function (sourceNode: any, targetNode: any) {
          // whether an edge can be created between source and target
          return !sourceNode.same(targetNode); // e.g. disallow loops
        },
        edgeParams: function (sourceNode: any, targetNode: any) {
          // for edges between the specified source and target
          // return element object to be passed to cy.add() for edge
          return { data: {} };
        },
        hoverDelay: 150, // time spent hovering over a target node before it is considered selected
        snap: true, // when enabled, the edge can be drawn by just moving close to a target node (can be confusing on compound graphs)
        snapThreshold: 50, // the target node must be less than or equal to this many pixels away from the cursor/finger
        snapFrequency: 15, // the number of times per second (Hz) that snap checks done (lower is less expensive)
        noEdgeEventsInDraw: true, // set events:no to edges during draws, prevents mouseouts on compounds
        disableBrowserGestures: true, // during an edge drawing gesture, disable browser gestures such as two-finger trackpad swipe and pinch-to-zoom
      };

      this.cy.edgehandles(edgehandlesDefaults);

      const contextMenuOptions: contextMenus.MenuOptions = {
        // Customize event to bring up the context menu
        // Possible options https://js.cytoscape.org/#events/user-input-device-events
        evtType: 'cxttap',
        // List of initial menu items
        // A menu item must have either onClickFunction or submenu or both
        // menuItems: [
        //   {
        //     id: 'remove', // ID of menu item
        //     content: 'remove', // Display content of menu item
        //     tooltipText: 'remove', // Tooltip text for menu item
        //     // image: { src: 'remove.svg', width: 12, height: 12, x: 6, y: 4 }, // menu icon
        //     // Filters the elements to have this menu item on cxttap
        //     // If the selector is not truthy no elements will have this menu item on cxttap
        //     selector: 'node, edge',
        //     onClickFunction: function () {
        //       // The function to be executed on click
        //       console.log('remove element');
        //     },
        //     disabled: false, // Whether the item will be created as disabled
        //     show: false, // Whether the item will be shown or not
        //     hasTrailingDivider: true, // Whether the item will have a trailing divider
        //     coreAsWell: false, // Whether core instance have this item on cxttap
        //     // submenu: [] // Shows the listed menuItems as a submenu for this item. An item must have either submenu or onClickFunction or both.
        //   },
        //   // {
        //   //   id: 'hide',
        //   //   content: 'hide',
        //   //   tooltipText: 'hide',
        //   //   selector: 'node, edge',
        //   //   onClickFunction: function () {
        //   //     console.log('hide element');
        //   //   },
        //   //   disabled: true,
        //   //   submenu: true,
        //   // }
        //   /*
        //   {
        //     id: 'add-node',
        //     content: 'add node',
        //     tooltipText: 'add node',
        //     image: {src : "add.svg", width : 12, height : 12, x : 6, y : 4},
        //     selector: 'node',
        //     coreAsWell: true,
        //     onClickFunction: function () {
        //       console.log('add node');
        //     }
        //   }*/,
        //   ,
        // ],
        // css classes that menu items will have
        menuItemClasses: [
          // add class names to this list
        ],
        // css classes that context menu will have
        contextMenuClasses: [
          // add class names to this list
        ],
        // Indicates that the menu item has a submenu. If not provided default one will be used
        // submenuIndicator: {
        //   src: 'assets/submenu-indicator-default.svg',
        //   width: 12,
        //   height: 12,
        // },
      };

      var instance = this.cy.contextMenus(contextMenuOptions);
    });

    // this.cy = cytoscape({
    //   container: document.getElementById('cy'),
    //   elements: [
    //     { data: { id: 'a' } },
    //     { data: { id: 'b' } },
    //     {
    //       data: {
    //         id: 'ab',
    //         source: 'a',
    //         target: 'b',
    //       },
    //     },
    //   ],
    // });
  }
}
