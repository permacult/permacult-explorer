import { TestBed } from '@angular/core/testing';

import { TaxNodeService } from './tax-node.service';

describe('TaxNodeService', () => {
  let service: TaxNodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaxNodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
