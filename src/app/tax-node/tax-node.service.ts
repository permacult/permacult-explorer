import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TaxNodeService {
  constructor(private http: HttpClient) {}

  findById(id: number): Observable<any> {
    let params = new HttpParams();

    id = +id;

    params = params
      .append('join', 'names')

      .append('join', 'rank')
      .append('join', 'division')

      .append('join', 'parent')
      .append('join', 'parent.names')
      .append('join', 'parent.rank')

      .append('join', 'parent.flatAncestors')
      .append('join', 'parent.ancestors.names')
      .append('ancestorsLimitRank', 'kingdom')

      .append('join', 'children')
      .append('join', 'children.rank')
      .append('join', 'children.names');

    try {
      return this.http.get<any>(environment.apiUrl + `/nlm/tax-node/` + id, {
        params: params,
      });
    } catch (e) {
      console.error(e);
    }

    return of([]);
  }
}
