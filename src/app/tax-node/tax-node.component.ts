import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import {
  startWith,
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
  finalize,
  catchError,
} from 'rxjs/operators';
import { TaxNodeService } from './tax-node.service';

@Component({
  selector: 'app-tax-node',
  templateUrl: './tax-node.component.html',
  styleUrls: ['./tax-node.component.scss'],
})
export class TaxNodeComponent implements OnInit {
  // private routeSub: Subscription;
  constructor(private route: ActivatedRoute, private service: TaxNodeService) {}

  // id: number | undefined;

  // taxNode$ = new BehaviorSubject<any>({});

  taxNode$ = of({ id: null });

  ngOnInit(): void {
    // const id = this.route.snapshot.paramMap.get('id');

    // if (id) {
    //   // this.id = +id;

    //   this.taxNode$ = this.service.findById(+id);
    // }

    this.route.params
      .pipe(
        map((param) => param['id']),
        distinctUntilChanged(),
        switchMap((id) => this.service.findById(id))
      )
      .subscribe((response) => {
        this.taxNode$ = of(response);
      });
  }

  getTaxNodeDetails() {}

  // findById(id: number): any {}

  // ngOnInit() {
  //   this.routeSub = this.route.params.subscribe(params => {
  //     console.log(params) //log the entire params object
  //     console.log(params['id']) //log the value of id
  //   });
  // }

  // ngOnDestroy() {
  //   this.routeSub.unsubscribe();
  // }
}
