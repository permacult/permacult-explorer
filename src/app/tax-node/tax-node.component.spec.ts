import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxNodeComponent } from './tax-node.component';

describe('TaxNodeComponent', () => {
  let component: TaxNodeComponent;
  let fixture: ComponentFixture<TaxNodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxNodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
