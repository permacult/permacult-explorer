import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShadowBoxDialogComponent } from './shadow-box-dialog.component';

describe('ShadowBoxDialogComponent', () => {
  let component: ShadowBoxDialogComponent;
  let fixture: ComponentFixture<ShadowBoxDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShadowBoxDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShadowBoxDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
