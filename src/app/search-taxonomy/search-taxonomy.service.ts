import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { TaxDivisionName } from './search-taxonomy.component';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SearchTaxonomyService {
  constructor(private http: HttpClient) {}

  searchByName({
    q,
    division = undefined,
  }: {
    q: string | undefined;
    division: TaxDivisionName | TaxDivisionName[] | undefined;
  }): Observable<any> {
    let params = new HttpParams();

    if (q === undefined) {
      return of([]);
    }

    params = params.append('q', q.replace(/^[\s]+|[\s]+$/g, '')); // trim
    params = params.append('join', 'names');
    params = params.append('join', 'division');
    params = params.append('join', 'parent');
    params = params.append('join', 'parent.names');

    params = params.append('join', 'parent.flatAncestors');
    params = params.append('join', 'parent.ancestors.names');
    params = params.append('ancestorsLimitRank', 'kingdom');

    params = params.append('join', 'children');
    params = params.append('join', 'children.names');

    // params = params.append('division', 'Plants and Fungi');
    const divisions: (string | undefined)[] = (
      Array.isArray(division) ? division : [division]
    )
      .filter((e) => e) // remove empty elements
      .map((e) => {
        const str = e?.toString();
        if (str) {
          params = params.append('division', str);
        }
        return str;
      });

    try {
      return this.http.get<any>(
        `${environment.apiUrl}/nlm/tax-node/findByName`,
        // `https://api.openpermacult.org/nlm/tax-node/findByName`,
        {
          params: params,
        }
      );
    } catch (e) {
      console.error(e);
    }

    return of([]);
  }
}
