import { Component, Input, OnInit } from '@angular/core';
// import {
//   FormBuilder,
//   FormControl,
//   FormGroup,
//   Validators,
// } from '@angular/forms';
import { SearchTaxonomyService } from './search-taxonomy.service';
import {
  startWith,
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
  finalize,
  catchError,
} from 'rxjs/operators';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ucFirst } from '../shared/utils/uc-first';

import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  NG_VALUE_ACCESSOR,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatIconRegistry } from '@angular/material/icon';

export enum TaxDivisionName {
  BACTERIA = 'Bacteria',
  INVERTEBRATES = 'Invertebrates',
  MAMMALS = 'Mammals',
  PHAGES = 'Phages',
  PLANTS_AND_FUNGI = 'Plants and Fungi',
  PRIMATES = 'Primates',
  RODENTS = 'Rodents',
  SYNTHETIC_AND_CHIMERIC = 'Synthetic and Chimeric',
  UNASSIGNED = 'Unassigned',
  VIRUSES = 'Viruses',
  VERTEBRATES = 'Vertebrates',
  ENVIRONMENTAL_SAMPLES = 'Environmental samples',
}

@Component({
  selector: 'app-search-taxonomy',
  templateUrl: './search-taxonomy.component.html',
  styleUrls: ['./search-taxonomy.component.scss'],
  providers: [
    // {
    //   provide: NG_VALUE_ACCESSOR,
    //   multi: true,
    //   useExisting: SearchTaxonomyComponent,
    // },
  ],
})
export class SearchTaxonomyComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private searchTaxonomyService: SearchTaxonomyService
  ) {}

  math = Math;
  ucFirst = ucFirst;

  form: FormGroup = this.fb.group({
    q: ['', []],
    division: [TaxDivisionName.PLANTS_AND_FUNGI, []],
  });

  // @Input()
  // divisionFormControl:FormControl;

  // @Input()
  // division = new FormControl('');

  // this.fb.group({
  //   formControlDivision: ['', Validators.required],
  // })

  // divisionFormOptions: TaxDivisionNa/me[] = Object.values(TaxDivisionName);
  divisionFormOptions: TaxDivisionName[] = Object.values(TaxDivisionName);
  // divisionFormOptions: TaxDivisionName[] = [
  //   TaxDivisionName.PLANTS_AND_FUNGI,
  //   TaxDivisionName.VIRUSES,
  // ];

  loading$ = new BehaviorSubject<boolean>(false);

  // found$: Observable<any[]> = of([]);

  // found$: Observable<any> = of([{ id: 'none' }]);
  found$ = new BehaviorSubject<any>([]);

  ngOnInit(): void {
    this.form.valueChanges
      .pipe(
        // startWith(''),
        debounceTime(300),
        distinctUntilChanged(),
        // filter((data) => data.trim().length >= 2),

        // switchMap((res) => {
        //   console.log('toto', res);
        //   return res;
        // })
        map((search) => {
          // console.log('toto', search);
          this.findByName(this.form.value);

          // .subscribe((r) => {
          //   console.log('r', r);
          //   return r
          // });

          // this.findByName(search);

          // this.found$ = this.findByName(search);
          // this.found$.subscribe();

          // console.log('result', result);
        })
        // switchMap((search) => {
        //   console.log(search);
        //   return this.findByName(search);
        // }),
        // tap(() => {
        //   this.loading$ = of(false);
        // })
      )
      .subscribe();
  }

  findByName(search: any) {
    console.log('findByName', search);

    if (search.q) {
      this.loading$.next(true);

      this.searchTaxonomyService
        .searchByName(search)
        .pipe(
          // debounceTime(300),
          // tap(() => {
          //   console.log('loading true');
          //   this.loading$.next(true);
          // }),
          map((res) => {
            this.found$.next(res);
            return res;
          }),
          // switchMap((res) => {
          //   return res.data;
          // }),

          // switchMap((res) => {
          //   console.log('toto', res);
          //   return res;
          // }),
          tap(() => {
            this.loading$.next(false);
          })
        )
        .subscribe
        // (data) => {
        //   this.found$.next(data);
        // },
        // () => {},
        // () => {
        //   console.log('loading false');
        //   this.loading$.next(false);
        // }
        ();
    } else {
      // this.found$ =  of([]);
    }
  }
}
